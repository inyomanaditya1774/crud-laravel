<?php

use App\Http\Controllers\karyawanCont;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [karyawanCont::class,'show']);
Route::get('/add', [karyawanCont::class,'show']);

Route::post('/simpan', [karyawanCont::class,'prosesSimpan']);

Route::get('/delete/{id}',[karyawanCont::class,'delete']);

Route::get('/edit/{id}',[karyawanCont::class,'edit']);

Route::post('/update/{id}',[karyawanCont::class,'update']);

