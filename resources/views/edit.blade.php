<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <title>EDIT DATA</title>
  </head>
  <body>
    
        <div class="container">    
            <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
                <div class="panel panel-info" >
                        <div class="panel-heading">
                            <div class="panel-title">Input data </div>
                        </div>     
    
                        <div style="padding-top:30px" class="panel-body" >
    
                            <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            @foreach ($user as $a)
                            <form id="loginform" class="form-horizontal" role="form" action="/update/{{ $a->id }}" method="POST">
                                {{ csrf_field() }}
                                        
                                        <div style="margin-bottom: 25px" class="input-group">
                                          
                                            <input id="login-username" type="text" class="form-control" name="nama_karyawan" value="{{ $a->nama_karyawan }}" placeholder="nama keryawan">                                        
                                        </div>
                                    
                                         <div style="margin-bottom: 25px" class="input-group">
                                    
                                            <input id="login-password" type="number" class="form-control" name="no_karyawan" placeholder="nomor karyawan" value="{{ $a->no_karyawan }}">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                        
                                            <input id="login-password" type="number" class="form-control" name="no_tlp_karyawan" placeholder="nomor telepon" value="{{ $a->no_tlp_karyawan }}">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                      
                                            <input id="login-password" type="text" class="form-control" name="jabatan_karyawan" placeholder="jabatan" value="{{ $a->jabatan_karyawan }}">
                                        </div>

                                        <div style="margin-bottom: 25px" class="input-group">
                                       
                                            <input id="login-password" type="text" class="form-control" name="divisi_karyawan" placeholder="divisi" value="{{ $a->divisi_karyawan }}">
                                        </div>
    
                                    <div style="margin-top:10px" class="form-group">
                                        <!-- Button -->
    
                                        <div class="col-sm-12 controls">
                                            <button type="submit" class="btn btn-primary">SUBMIT</button>
    
                                        </div>
                                    </div>

                                    </div>    
                                </form>     
                                @endforeach
                            </div>                     
                        </div>  
            </div>

        </div>  
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>